---
title: DOCBOX - Markdown转PDF/DOCX/HTML
author: Iceway Zhang
---

# 功能

- 构建一个docker镜像，包含pandoc，wkhtmltopdf工具的文档格式转换工具。
- 提供一个脚本工具docbox封装pandoc和wkhtmltopdf的具体用法，方便从markdown到html和pdf的转换。
- markdown转换html时，pandoc参数关闭代码高亮，然后把prism.js插入到生成的html页面，由prism提供高亮功能年。

# 安装

1. 构建docker镜像。

```bash
./setup.sh --build
```

2. 安装docbox到本机。

> 仅测试过Ubuntu 20.04系统。

```bash
./setup.sh --install
```

# 用法

执行 `docbox -h` 查看具体用法。


# markdown的一些写法示例

除以下特殊写法外，均使用标准markdown语法。

## 头部元信息

建议在markdown文件开始处加上以下元信息，docbox在转换时会自动将这些内容提取生成封面信息。

``` yaml
---
title: title string
subtitle: subtitle string
author: author
date: date and time
---
```

## prism代码高亮及插件

prism支持的代码高亮，已经提供的一些插件，增强代码块的显示效果。

**指定代码段所用语言**

```` markdown
``` bash
````

或

```` markdown
``` {.bash}
````

**在代码段左边显示行数**

```` markdown
``` {.bash .line-numbers}
````

**代码段左边显示行数，并且高亮指定行**

```` markdown
``` {.bash .line-numbers data-line="2,3,7-10.16"}
````

**显示命令及其执行结果**

```` markdown
``` {.bash .command-line data-prompt="user@host $ " data-output="2-3,6,8"}
````

## vuepress 提示块样式

markdown示例

```` markdown
::: tip
tips text
:::

::: warning
warning text
:::

::: danger
danger text
:::
````
