# Changelog

## 1.3.21 (2021/11/7)

- (refactor): update typohan.css to adjust some style.
- (refactor): only store docbox in this repository, and will move docbox_online to another repo.
- (refactor): force change docbox version to 1.3.21.

## 3.21 (2021/8/25)

- (feat): upgrade typohan.css to latest support colorful div and span.
- (docs): update new samples in docbox_online index page to use new feature in typohan.css
- (feat): add extra header and footer for pdf if is_commerical
- (fix): increase timeout for nginx cgi

## 3.20 (2021/8/24)

- (feat): upgrade typohan.css to latest
- (refactor): remove custom.css as it's integrated into typohan.css
- (feat): update md_process.py to process document tags '#xxx' to customized spans
- (docs): adjust index page of docbox_online

## 3.19 (2021/8/23)

- (fix): fix that can't handle cover page if input is dos format (crlf) in docbox
- (feat): define span.mark to same as <mark> in custom.css

## 3.18 (2021/8/18)

- (fix): fix setup.sh could not install locally correct.
- (feat): add command to run docbox_online container in setup.sh
- (fix): fix that md2x.sh in webserver could not pack converted file which name include space

## 3.16 (2021/8/16)

- (refactor): rename some web resource files for commerical using.
- (fix): fix 'md_process.py input.md | xxx' fail in pipeline.
- (refactor): [BREAKING CHAGE]: rewrite docbox
- (fix): adjust cover style for md2html
- (fix): update pandoc_md_filter.py and docbox to only embeded plantuml converted image to data-uri but not origin image in markdown
- (refactor): use pandoc inside code highlight to render code block for md2docx
- (fix): fix 2 topos.
- (fix): update webserver cgi tools to use latest options of docbox
- (fix): fix output file missing flantuml converted image.
- (chore): upgrade docbox_online to 3.16

## 3.15 (2021/8/13)

- (feat): remove wiki format supporting from docbox tool
- (fix): install pandoc and pandoc-plantuml-filter via apt as it's latest in ubuntu 20.04
- (fix): skip \`\`\`plantuml block as it will be rendered by pandoc-plantuml-filter in md
- (feat): add my pandoc filter to process markdown to html
- (fix): pandoc md2html: change output format from html5 to html then plantuml will convert to svg but not png
- (feat): add my markdown pre process tool
- (refactor): use my tool md_process.py and pandoc_md_filter.py in docbox
- (docs): update index page of docbox_online to introduce plantuml

## 3.14 (2021/8/9)

- (fix): upgrade pandoc to 2.14.1 to fix pandoc 2.12 parse grid table incorrectly with chinese/english mixed title.
- (docs): update index page of docbox_online to introduce vscode + table formatter to write grid table.

## 3.13 (2021/8/6)

- (refactor): use pandoc fenced_divs for custom-block '::: XXX'
- (feat): add new subcmd md2md to convert origin markdown to pre-handle customized markdown
- (feat): add new span with class tip|warning|danger to custom.css
- (refactor): adjust colors of tip class in customized span and block
- (feat): adjust docbox online tool's index page to add more introduce
- prism.js: upgrade to 1.24.1 and add new plugin 'Show Language'
- (style): docbox md2html to convert index.md to index.html with okaidia style.
- (fix): fix docbox_online cgi run fail which imported in commit f3d0878

## 3.12 (2021/8/5)

- (refactor): add a new function to handle my markdown extension
- (feat): docbox support new custom block 'output' style.
- (docs): update docbox online index page
- (fix): adjust left and right margin of each page for wkhtmltopdf from 25 to 20
- (feat): upgrade typohan.css to adjust inline code style to not set font color as soft-red
- (refactor): restore to use docbox_online as docker image name
- prism css: change word-break from normal to break-all
- (refactor): remove cover page background picture and use a normal logo for non-delta
- (refactor): adjust Dockerfile to not install md2pdf tool and move fixed label to head

## 3.11 (2021/7/30)

- change height of `<table>` to normal in topyhan.css.
- docbox online tool support file name include space and chinese character.
- add more introduce in index web page of online tool.

## 3.10 (2021/7/29)

- remove margin-bottom for ul,ol in typehan.css
- merge docbox_online image into docbox image.

## 3.9 (2021/7/21)

- fix md2pdf will show long line in code blocks abnormal.

## 3.8 (2021/7/14)

- docbox/docbox_online support watermark (powered by pdftk).
- adjust margin between title and meta in cover page for pdf.
- upgrade docker base image to ubuntu 12.04.
- upgrade sarasa font to 0.32.10

## 3.7 (2021/7/14)

- upgrade docbox to 2.1 to adjust style of cover page for md2pdf.
- upgrade prism.js to 1.24.1

## 3.6 (2021/4/8)

- update typohan.css to adjust styple of inline code.

## 3.5 (2021/3/15)

- update wkhtmltopdf toc xsl style.

## 3.4 (2021/3/12)

- fix Noto-fonts always select JP by default.
- upgrade pandoc to 2.12 and sarasa font to 0.18.4

## 3.3 (2021/3/12)

- update typohan.css to adjust style of inline code.

## 3.2 (2020/10/23)

- update typohan.css to adjust style for img,figure,figcaption.

## 3.1 (2020/10/22)

- update typohan.css to adjust style for link and table.
- do not specify english fonts in typohan.css.
- revert to use official wkhtmltopdf.

## 3.0 (2020/9/30)

- docbox: support markdown to docx.
- docbox_online: support new option for markdown to html/docx.

## 2.9 (2020/9/29)

- use Noto sans/serif as first fonts.

## 2.8 (2020/9/27)

- use my built wkhtmltox with patched qt (fixed font fallback issue).
- adjust fonts in typohan.css

## 2.6 (2020/9/23)

- install fonts-arphic-ukai and update typohan.css to use ukai as serif font.

## 2.5 (2020/9/16)

- upgrade docbox script to 1.6 to support vuepress custom blocks.

## 2.2 (2020/7/10)

- upgrade docbox script to 1.3 to generate unique temp files and remove after converted.

## 2.1 (2020/7/9)

- do not install texlive by default.

## 2.0 (2020/7/9)

- adjust margin of page in md2pdf.
- add new option to install docbox in local.
- add option to control outline depth for wkhtmltopdf.

## 1.9 (2020/7/7)

- upgrade docbox script to 1.2, use wkhtmltopdf genearate toc for md2pdf.

## 1.8 (2020/6/23)

- refactor run_docker.sh and rename is to setup.sh
- add texlive.
- add new tool md2pdf to convert markdown file to pdf user pandoc+xelatex.

## 1.7 (2020/6/22)

- upgrade pandoc to 2.9.2.1
- upgrade wkhtmltopdf to 0.12.6-rc1
- move all tools/resources to sub-folder resource.
- use "Sarasa mono" as code block's font.
- use font-noto-cjk as defualt serif and sans fonts.
- add a download folder to store downloaded files as cache to build docker image.
- adjust margin of pages in wkhtmltopdf.

## V1.6 (2020/2/28)

- refactor docbox script.
- upgrade pandoc to 2.9.2.
- upgrade prism.js to 1.19.0.
- remove header of each page from pdf when convert by wkhtmltopdf.

## V1.5 (2019/2/22)

- update default style for cover to convert to pdf by wkhtmltopdf.
- update cover style with typohan.css.
- insert contents of css and js to html but not links if no arg_uripath.
- remove default ENV for uripath to let insert css and js.
