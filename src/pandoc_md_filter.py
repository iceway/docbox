#!/usr/bin/env python3

"""
Pandoc filter to process markdown:
    1. plantuml code block replace by plant-generated images.
        - Needs `plantuml.jar` from http://plantuml.com/.
        - Refer to https://github.com/jgm/pandocfilters/blob/master/examples/plantuml.py
    2. other code block convert class language name xxx to language-xxx.
    3. Div block without class custom-block, will add it.
"""

import os
import sys
import subprocess
import base64

from pandocfilters import toJSONFilter, Para, Image, CodeBlock, Div
from pandocfilters import get_filename4code, get_caption, get_extension

PLANTUML_BIN = os.environ.get("PLANTUML_BIN", "plantuml")


def my_markdown(key, value, format_, _):
    if key == "CodeBlock":
        # example:
        # ["bash-id", ["bash", "line-numbers"], [["data-start", "10"], ["data-line", "10,15-18"]]]
        [[id, classes, keyvals], code] = value

        if format_ == "html" or format_ == "markdown":
            if "plantuml" in classes:
                # replace ```plantuml or ```{.plantuml} to digram rendered by plantuml.jar
                caption, typef, keyvals = get_caption(keyvals)

                filename = get_filename4code("plantuml", code)
                filetype = get_extension(format_, "png", html="svg", latex="png")

                src = filename + ".uml"
                dest = filename + "." + filetype

                uri_img_type = {"png": "png", "svg": "svg+xml"}
                img_b64 = ""

                if not os.path.isfile(dest):
                    txt = code.encode(sys.getfilesystemencoding())
                    if not txt.startswith(b"@start"):
                        txt = b"@startuml\n" + txt + b"\n@enduml\n"
                    with open(src, "wb") as f:
                        f.write(txt)

                    subprocess.check_call([PLANTUML_BIN, "-t" + filetype, src])
                    sys.stderr.write("Created image " + dest + "\n")
                with open(dest, "rb") as f:
                    img_b64 = base64.standard_b64encode(f.read()).decode("ascii")

                img_uri = "data:image/{0};base64,{1}".format(
                    uri_img_type[filetype], img_b64
                )

                return Para([Image([id, [], keyvals], caption, [img_uri, typef])])
            else:
                _classes = []
                for x in classes:
                    _classes.append(x)
                    if x == "command-line":
                        # prism.js 插件支持的 class
                        pass
                    elif x == "line-numbers" and not "numberLines" in classes:
                        _classes.append("numberLines")
                    elif x == "numberLines" and not "line-numbers" in classes:
                        # 代码行号是 pandoc 支持的 class，追加 prism.js 支持的代码行号 class
                        _classes.append("line-numbers")
                    else:
                        if x.startswith("language-"):
                            _classes.append(x.replace("language-", ""))
                        else:
                            # 所有其他不是以 language- 开头的 class，默认是代码语言标识,
                            # 追加 prism.js 支持的代码编程语言 class 。
                            _classes.append(f"language-{x}")

                _keyvals = []
                for k, v in keyvals:
                    _keyvals.append([k, v])
                    if k == "startFrom":
                        # 如果是 pandoc 支持的代码行编号开始值，追加 prism.js 支持的代码行编号开始值。
                        _keyvals.append(["data-start", v])
                    if k == "data-start":
                        _keyvals.append(["startFrom", v])

                return CodeBlock([id, _classes, _keyvals], code)
        elif format_ == "docx":
            if "line-numbers" in classes and not "numberLines" in classes:
                classes.append("numberLines")

            if "data-start" in keyvals and not "startFrom" in keyvals:
                keyvals["startFrom"] = keyvals["data-start"]

            return CodeBlock([id, classes, keyvals], code)
    elif key == "Div":
        [[id, classes, keyvals], content] = value

        # my customized div block
        # tip, info, warning, danger, output,
        if "custom-block" not in classes:
            classes.append("custom-block")

        return Div([id, classes, keyvals], content)


def main():
    toJSONFilter(my_markdown)


if __name__ == "__main__":
    main()
