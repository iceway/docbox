#!/usr/bin/env bash

if [[ $UID -ne 0 ]]; then
	echo "Please run as root!"
	exit 1
fi

curdir="$(dirname "$(readlink -f "$0")")"
gh_proxy="https://ghproxy.com/"

if ! command -v pdftk >/dev/null || ! command -v pandoc >/dev/null; then
	apt-get update -y \
		&& apt-get install -y --no-install-recommends \
			p7zip-full wget ca-certificates dos2unix \
			locales fontconfig fonts-noto-cjk fonts-noto-color-emoji \
			libgmp10 libxext6 \
			xfonts-base xfonts-75dpi libjpeg-turbo8 libxrender1 \
			perl perl-modules-5.30 libperl5.30 \
			pdftk \
			pandoc pandoc-plantuml-filter python3-pandocfilters
fi

# ubuntu中文Noto字体默认使用日语汉字，修改为简体中文汉字。
if ! grep -E 'Noto Sans CJK SC' "$HOME/.fonts.conf" >/dev/null 2>&1; then
	echo ">>> create $HOME/.fonts.conf to correct use SC as default of Noto CJK fonts."
	cat >"$HOME/.fonts.conf" <<-EOF
		<?xml version="1.0"?>
		<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
		<fontconfig>
			<alias>
				<family>sans-serif</family>
				<prefer>
					<family>Noto Sans CJK SC</family>
					<family>Noto Sans CJK TC</family>
					<family>Noto Sans CJK HK</family>
					<family>Noto Sans CJK JP</family>
					<family>Noto Sans CJK KR</family>
					<family>Lohit Devanagari</family>
				</prefer>
			</alias>
			<alias>
				<family>serif</family>
				<prefer>
					<family>Noto Serif CJK SC</family>
					<family>Noto Serif CJK TC</family>
					<family>Noto Serif CJK JP</family>
					<family>Noto Serif CJK KR</family>
					<family>Lohit Devanagari</family>
				</prefer>
			</alias>
			<alias>
				<family>monospace</family>
				<prefer>
					<family>Noto Sans Mono CJK SC</family>
					<family>Noto Sans Mono CJK TC</family>
					<family>Noto Sans Mono CJK HK</family>
					<family>Noto Sans Mono CJK JP</family>
					<family>Noto Sans Mono CJK KR</family>
				</prefer>
			</alias>
		</fontconfig>
	EOF
fi

# tools: wkhtmltopdf: 将html转为pdf的工具（主要是为了支持丰富的CSS）。
if ! command -v wkhtmltopdf >/dev/null 2>&1; then
	echo ">>> download and install wkhtmltopdf."
	version="0.12.7-0.20220106.30.dev.8c40d9a"
	target="wkhtmltox_${version}.focal_amd64.deb"
	url="${gh_proxy}https://github.com/iceway/wkhtmltopdf/releases/download/v${version}/${target}"
	wget -nv "$url" -O "$target" \
		&& dpkg -i "$target" \
		&& rm -f "$target"
fi

# fonts: sarasa (更纱黑体）可以用于代码块的字体，包含中英文（等宽）。
font_dir="/usr/share/fonts/truetype/sarasa"
if ! fc-query "$font_dir"/sarasa-regular.ttc | grep -E 'family:.*等距更纱黑体 SC' >/dev/null 2>&1; then
	echo ">>> download and install sarasa font."
	version="0.34.7"
	target="sarasa-gothic-ttc-${version}.7z"
	url="${gh_proxy}https://github.com/be5invis/Sarasa-Gothic/releases/download/v${version}/$target"
	mkdir -p "$font_dir" \
		&& wget -nv "$url" -O "$target" \
		&& 7z x -o"$font_dir" "$target" \
		&& rm -f "$target"
fi

if ! locale | grep 'LANG=en_US.UTF-8' >/dev/null 2>&1; then
	echo ">>> locale-gen UTF-8 env."
	locale-gen en_US.UTF-8
	export LANG="en_US.UTF-8"
fi

### copy docbox and local resources.
if [[ ! -d /opt/docbox ]]; then
	mkdir -p /opt/docbox
fi
if [[ ! -L /usr/local/bin/docbox ]]; then
	echo ">>> create softlink for docbox"
	ln -sf /opt/docbox/docbox /usr/local/bin/docbox
fi
echo ">>> copy all docbox and resources to /opt/docbox/"
cp -rf "$curdir"/* /opt/docbox/
