#!/usr/bin/env bash

gen_svg_list_page()
{
cat <<-EOF
<html>
  <head>
    <meta charset="UTF-8" />
    <style type="text/css">
      div.box {
        display: inline-block;
        margin: 10px 5px;
        border: 1px solid gainsboro;
      }
      img {
        max-width: 100mm;
        max-height: 100mm;
      }
      div p {
        font-size: 32px;
        text-align: center;
        background-color: gold;
        margin-bottom: 0;
      }
    </style>
  </head>
  <body>
EOF

for n in $(ls -1 svg/*.svg); do
	b="${n#svg/}"
	echo "<div class=\"box\"><img src=\"$n\" /><p>${b%.svg}</p></div>"
done

cat <<-EOF
  </body>
</html>
EOF
}

gen_svg_list_page >svg-list.html
