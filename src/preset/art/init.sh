#!/usr/bin/env bash

curdir="$(dirname "$(readlink -f "$0")")"
svgdir="${docbox_dir:?missing docbox_dir}/lib/svg"

declare -a svg_files

if [[ -z "$SVGFILE" ]]; then
	mapfile -t svg_files < <(cd "$svgdir" && ls -1 *.svg)
	idx="$((RANDOM % ${#svg_files[@]}))"
	SVGFILE="file://$(readlink -f "${svgdir}/${svg_files[$idx]}")"
fi

sed -ri "s#@@SVGFILE@@#$SVGFILE#" "$curdir/cover.html"
