#!/usr/bin/env bash

curdir="$(dirname "$(readlink -f "$0")")"

YEAR="$(date +%Y)"

sed -ri "s#@@YEAR@@#$YEAR#" "$curdir/cover.html"
