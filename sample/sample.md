---
title: DOCBOX Markdown 说明
subtitle: A Sample Markdown Usage
version: V1.1
author: Iceway Zhang
date: 2022/5/11
---

# 如何在生成的 PDF 文件中定制封面？

本工具支持 PDF 封面内容，封面一般包含的是文档标题，作者，日期等信息，要生成包含封面的 PDF 文件，需要在上传的 Markdown 文件最开始的几行包含以下元信息：

- 目前本工具只支持以下元信息，要想生成的 PDF 文件带有封面，必须要有 title 项，其他可选。
- 这些内容必须是写在 markdown 文件最开始的地方。
- 这些信息前后的`---`由三个短横线组成的行是必须的。

```yaml
---
title: 文档标题
subtitle: 文档副标题
version: 文档版本
author: 作者信息
date: 日期
---
```

# Markdown 文档要怎么写？

Markdown 是一种简洁的标记语言，设计目的就是用于写作文本文档的，目前已经成为了 github，gitlab 等网站的默认文档格式，用于写 release note，README 及其他的文档非常适合。

基本的 Markdown 语法可以从参考 Markdown 语法说明[^MarkdownSyntax]，另外可以从这里了解 Markdown 基本语法[^MarkdownBasis]和 Markdown 扩展语法[^MarkdownExtend]。

## 基础

这是*斜体*，这是**粗体**，这是~~删除线~~。

## 引用

源码：

```markdown
> 设计模式六大原则：
>
> - 开闭原则(Open Close Principle)
> - 里氏代换原则(Liskov Substitution Principle)
> - 依赖倒转原则(Dependence Inversion Principle)
> - 接口隔离原则(Interface Segregation Principle)
> - 迪米特法则(最少知道原则)(Demeter Principle)
> - 合成复用原则(Composite Reuse Principle)
```

生成内容：

> 设计模式六大原则：
>
> - 开闭原则(Open Close Principle)
> - 里氏代换原则(Liskov Substitution Principle)
> - 依赖倒转原则(Dependence Inversion Principle)
> - 接口隔离原则(Interface Segregation Principle)
> - 迪米特法则(最少知道原则)(Demeter Principle)
> - 合成复用原则(Composite Reuse Principle)

## 图片

源码：

```markdown
![希里](Ciri.jpg)
```

生成内容：

![希里](Ciri.jpg){width=50%}

## 表格

源码：

```markdown
| Right | Left | Default | Center |
| ----: | :--- | ------- | :----: |
|    12 | 12   | 12      |   12   |
|   123 | 123  | 123     |  123   |
|     1 | 1    | 1       |   1    |
```

生成内容：

| Right | Left | Default | Center |
| ----: | :--- | ------- | :----: |
|    12 | 12   | 12      |   12   |
|   123 | 123  | 123     |  123   |
|     1 | 1    | 1       |   1    |

# 本工具支持的 Markdown 扩展用法

## 链接到标题

在文档中要链接到某个标题，可以用语法：`[要链接的标题内容]` 或者 `[显示内容][要链接的标题内容]`。

```markdown
链接到标题：[表格]

链接到标题：[转换多个 Markdown][怎么转换多个 markdown 文件？]
```

显示效果：

::: output
链接到标题：[表格]

链接到标题：[转换多个 Markdown][怎么转换多个 markdown 文件？]
:::

## 支持单元格内多行内容的表格

_表格标题：在表格前或者后面出现的 `Table:` 或者 `:` 开始的行将会作为表格标题。_

源码：

```markdown
<!-- prettier-ignore -->
+----------+-----------+-----------------------------------------------------+
| 版本     | 日期      | 改动内容                                            |
+:========:+:=========:+:====================================================+
| V1.0.0.1 | 2021/5/10 | - fix bug #12345 : can't access web page sometimes. |
|          |           | - upgrade sub-modules.                              |
+----------+-----------+-----------------------------------------------------+
| V1.0.0.2 | 2021/7/20 | - add new feature AAA and BBB.                      |
|          |           | - remove useless modules as customer required.      |
|          |           | - fix security issue of command line injection. \   |
|          |           | long line long line long line.                      |
|          |           | - fix security issue of command line injection.     |
+----------+-----------+-----------------------------------------------------+

Table: uboot 版本修改历史
```

生成内容：

<!-- prettier-ignore -->
+----------+-----------+-----------------------------------------------------+
| 版本     | 日期      | 改动内容                                            |
+:========:+:=========:+:====================================================+
| V1.0.0.1 | 2021/5/10 | - fix bug #12345 : can't access web page sometimes. |
|          |           | - upgrade sub-modules.                              |
+----------+-----------+-----------------------------------------------------+
| V1.0.0.2 | 2021/7/20 | - add new feature AAA and BBB.                      |
|          |           | - remove useless modules as customer required.      |
|          |           | - fix security issue of command line injection. \   |
|          |           | long line long line long line.                      |
|          |           | - fix security issue of command line injection.     |
+----------+-----------+-----------------------------------------------------+

Table: uboot 版本修改历史

::: tip
**技巧**

上述表格是 RST 的 Grid Table 格式，这种格式对每列内容的字符宽度有严格要求，如果直接手工撰写很容易出错，建议使用 [VS Code 编辑器 + Table Formatter 插件]{.warning} 编辑 Grid Table。

比如，编写的原始内容如下：

```markdown
+-
| 日期 | 人员 | 修改历史 |
+=
| 2021/8/1 | Iceway.Zhang | - 发布第一版内容 |
+-
| 2021/8/8 | Iceway.Zhang | - 修正 Issue #1 |
| | | - 添加新功能，支持定制化的 div 和 span，需要使用特定语法撰写。 \ |
| | | 比如 span 语法为：`[span的内容范围]{.tip}`。 |
| | | - 更新使用文档，介绍更多扩展的用法。|
+-
| | | `{.language-bash} | | | | ls -lha $HOME | | | | find $HOME -name *rc -exec mv {} {}.bak \; | | | |` |
+-
```

:::

## 特殊样式块

> 请注意，**块**的概念可以理解为一个独立的段落，所以[特殊样式块的语法需要在 ::: xxx 前和 ::: 后分别保留空白行]{.danger}，否则 Markdown 解析可能会将这部分特殊语法语句认为是前面段落的续接部分，而导致渲染出错误的结果。

```markdown
::: output
模拟窗口应用（如浏览器等）的显示，适合普通输出显示等。
:::

::: faq
这是问题部分？

这是答案部分，FAQ 问答样式，显著提醒问答内容。
:::

::: tip
淡绿色背景+左侧绿色框线，加重深绿色字体，适合提示信息等。
:::

::: info
淡蓝色背景+左侧蓝色框线，加重深蓝色字体，适合一般信息等。
:::

::: warning
淡黄色背景+左侧深黄色框线，加重深黄色字体，适合警告信息等。
:::

::: danger
淡红色背景+左侧深红色框线，加重深红色字体，适合错误信息等。
:::

::: red
这是一个特殊的彩色块，用于特殊提醒。
:::

::: orange
这是一个特殊的彩色块，用于特殊提醒。
:::

::: yellow
这是一个特殊的彩色块，用于特殊提醒。
:::

::: green
这是一个特殊的彩色块，用于特殊提醒。
:::

::: blue
这是一个特殊的彩色块，用于特殊提醒。
:::

::: violet
这是一个特殊的彩色块，用于特殊提醒。
:::
```

转换后显示如下：

::: output
模拟窗口应用（如浏览器等）的显示，适合普通输出显示等。
:::

::: faq
这是问题部分？

这是答案部分，FAQ 问答样式，显著提醒问答内容。
:::

::: tip
淡绿色背景+左侧绿色框线，加重深绿色字体，适合提示信息等。
:::

::: info
淡蓝色背景+左侧蓝色框线，加重深蓝色字体，适合一般信息等。
:::

::: warning
淡黄色背景+左侧深黄色框线，加重深黄色字体，适合警告信息等。
:::

::: danger
淡红色背景+左侧深红色框线，加重深红色字体，适合错误信息等。
:::

::: red
这是一个特殊的彩色块，用于特殊提醒。
:::

::: orange
这是一个特殊的彩色块，用于特殊提醒。
:::

::: yellow
这是一个特殊的彩色块，用于特殊提醒。
:::

::: green
这是一个特殊的彩色块，用于特殊提醒。
:::

::: blue
这是一个特殊的彩色块，用于特殊提醒。
:::

::: violet
这是一个特殊的彩色块，用于特殊提醒。
:::

## 段内样式

**段内代码**

```markdown
段落内用单个`反引号包围的内容`会显示为段内代码样式。
```

显示效果：

::: output
段落内用单个`反引号包围的内容`会显示为段内代码样式。
:::

**删除线**

```markdown
段落内用~~两个波浪符包围的内容~~会显示为删除线样式。
```

显示效果：

::: output
段落内用~~两个波浪符包围的内容~~会显示为删除线样式。
:::

**上标和下标**

```markdown
段落内用单个 `^` 包围的内容显示为上标，如：2^10^; 用单个波浪符 `~` 包围的内容显示为下标，如：H~2~O。
```

显示效果:

::: output
段落内用单个 `^` 包围的内容显示为上标，如：2^10^; 用单个波浪符 `~` 包围的内容显示为下标，如：H~2~O。
:::

**段内特殊样式**

```markdown
Markdown 标准的段内特殊样式是：_斜体_, **粗体**, **_粗斜体_** 。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.mark} 。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.tip} 。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.info}。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.warning} 。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.danger}。

在段落内部[这是一个特殊的彩色块，用于强调。]{.red}

在段落内部[这是一个特殊的彩色块，用于强调。]{.orange}

在段落内部[这是一个特殊的彩色块，用于强调。]{.yellow}

在段落内部[这是一个特殊的彩色块，用于强调。]{.green}

在段落内部[这是一个特殊的彩色块，用于强调。]{.blue}

在段落内部[这是一个特殊的彩色块，用于强调。]{.violet}
```

显示效果：

::: output
Markdown 标准的段内特殊样式是：_斜体_, **粗体**, **_粗斜体_** 。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.mark} 。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.tip} 。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.info}。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.warning} 。

段内也可以使用[特殊强调的内容用，可以使用此处的样式]{.danger}。

在段落内部[这是一个特殊的彩色块，用于强调。]{.red}

在段落内部[这是一个特殊的彩色块，用于强调。]{.orange}

在段落内部[这是一个特殊的彩色块，用于强调。]{.yellow}

在段落内部[这是一个特殊的彩色块，用于强调。]{.green}

在段落内部[这是一个特殊的彩色块，用于强调。]{.blue}

在段落内部[这是一个特殊的彩色块，用于强调。]{.violet}
:::

# 特殊技巧

**连续两个有序列表如何重新开始序列号？**

```markdown
1. list item
2. list item
3. list item

<!-- -->

1. list item
2. list item
3. list item
```

显示效果：

::: output

1. list item
2. list item
3. list item

<!-- -->

1. list item
2. list item
3. list item

:::

# 如何在文档中高亮代码块？

## 代码块语法

如下所示，用行 ` ``` <代码块所用语言> ` 和行 ` ``` ` 包围的段落，会被显示为独立代码块，块中的内容会按照指明的语言的语法解析并高亮显示。

`<代码块所用语言>` 支持常见的各种编程语言等，如: c、shell、python、js、html、css、makefile、json、xml、csv、svg、sql、diff ...

````markdown
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_bin(void *v, int len)
{
	unsigned char bit, *p = (unsigned char *)v;
	int i, j;

	printf("bin(0b):");
	for (i = len - 1; i >= 0; i--) {
		for (j = 7; j >= 0; j--) {
			if (j == 7 || j == 3)
				printf(" ");
			bit = (p[i] >> j) & 1;
			printf("%u", bit);
		}
	}

	printf("\n");
}
```
````

显示效果：

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_bin(void *v, int len)
{
	unsigned char bit, *p = (unsigned char *)v;
	int i, j;

	printf("bin(0b):");
	for (i = len - 1; i >= 0; i--) {
		for (j = 7; j >= 0; j--) {
			if (j == 7 || j == 3)
				printf(" ");
			bit = (p[i] >> j) & 1;
			printf("%u", bit);
		}
	}

	printf("\n");
}
```

## 代码块前显示行号

在代码块开始行最后加上 `{.line-numbers}`，将会自动在代码块前显示代码行号。默认的行号从 1 开始，如果要指定开始行号，需在大括号中同时指定 data-start 属性，如 `{.line-numbers data-start="100"}`。

````markdown
```c {.line-numbers data-start="11"}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_bin(void *v, int len)
{
	unsigned char bit, *p = (unsigned char *)v;
	int i, j;

	printf("bin(0b):");
	for (i = len - 1; i >= 0; i--) {
		for (j = 7; j >= 0; j--) {
			if (j == 7 || j == 3)
				printf(" ");
			bit = (p[i] >> j) & 1;
			printf("%u", bit);
		}
	}

	printf("\n");
}
```
````

显示效果：

```c {.line-numbers data-start="11"}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_bin(void *v, int len)
{
	unsigned char bit, *p = (unsigned char *)v;
	int i, j;

	printf("bin(0b):");
	for (i = len - 1; i >= 0; i--) {
		for (j = 7; j >= 0; j--) {
			if (j == 7 || j == 3)
				printf(" ");
			bit = (p[i] >> j) & 1;
			printf("%u", bit);
		}
	}

	printf("\n");
}
```

## 高亮显示代码块中的指定行

用 data-line 属性指定需要高亮显示的行，如 `{.line-numbers data-line="10,15-20,30"}` 将高亮显示第 10 行、第 15 至 20 行、第 30 行。

````markdown
```c {.line-numbers data-line="17,22-27" data-start="11"}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_bin(void *v, int len)
{
	unsigned char bit, *p = (unsigned char *)v;
	int i, j;

	printf("bin(0b):");
	for (i = len - 1; i >= 0; i--) {
		for (j = 7; j >= 0; j--) {
			if (j == 7 || j == 3)
				printf(" ");
			bit = (p[i] >> j) & 1;
			printf("%u", bit);
		}
	}

	printf("\n");
}
```
````

显示效果：

```c {.line-numbers data-line="17,22-27" data-start="11"}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_bin(void *v, int len)
{
	unsigned char bit, *p = (unsigned char *)v;
	int i, j;

	printf("bin(0b):");
	for (i = len - 1; i >= 0; i--) {
		for (j = 7; j >= 0; j--) {
			if (j == 7 || j == 3)
				printf(" ");
			bit = (p[i] >> j) & 1;
			printf("%u", bit);
		}
	}

	printf("\n");
}
```

# PlantUML 绘图

在 Markdown 文件中，将 plantuml 语句嵌入到 代码块中，并标识语言为 plantuml，转换时将自动渲染成图片并替换源 plantuml 代码块，如：

````markdown
```plantuml
@startuml

actor Utilisateur as user
participant "formSign.js" as form <<Contrôleur formulaire>>
participant "Sign.java" as controler <<(C,#ADD1B2) Contrôleur formulaire>>
participant "Secure.java" as secure <<(C,#ADD1B2) authentification>>
participant "Security.java" as security <<(C,#ADD1B2) sécurité>>

box "Application Web" #LightBlue
	participant form
end box

box "Serveur Play" #LightGreen
	participant controler
	participant secure
	participant security
end box

user -> form : submitSignIn()
form -> form : getParameters()
form -> form : result = checkFields()

alt result

    form -> controler : formSignIn(email,pwd)
    controler -> controler : result = checkFields()

    alt result
    	controler -> secure : Secure.authenticate(email, pwd, true);
    	secure -> security : onAuthenticated()
    	security --> form : renderJSON(0);
    	form --> user : display main page
    else !result
    	controler --> form : renderJSON(1)
    	form --> user : display error
    end

else !result
	form --> user : display error
end

@enduml
```
````

将会被转成：

```plantuml
@startuml

actor Utilisateur as user
participant "formSign.js" as form <<Contrôleur formulaire>>
participant "Sign.java" as controler <<(C,#ADD1B2) Contrôleur formulaire>>
participant "Secure.java" as secure <<(C,#ADD1B2) authentification>>
participant "Security.java" as security <<(C,#ADD1B2) sécurité>>

box "Application Web" #LightBlue
	participant form
end box

box "Serveur Play" #LightGreen
	participant controler
	participant secure
	participant security
end box

user -> form : submitSignIn()
form -> form : getParameters()
form -> form : result = checkFields()

alt result

    form -> controler : formSignIn(email,pwd)
    controler -> controler : result = checkFields()

    alt result
    	controler -> secure : Secure.authenticate(email, pwd, true);
    	secure -> security : onAuthenticated()
    	security --> form : renderJSON(0);
    	form --> user : display main page
    else !result
    	controler --> form : renderJSON(1)
    	form --> user : display error
    end

else !result
	form --> user : display error
end

@enduml
```

[^MarkdownSyntax]: 杨帆.Markdown 语法说明（详解版）.(2011-11-03): <https://www.ituring.com.cn/article/504>
[^MarkdownBasis]: Markdown Basic Syntax: <https://www.markdownguide.org/basic-syntax/>
[^MarkdownExtend]: Markdown Extended Syntax: <https://www.markdownguide.org/extended-syntax/>
