#!/usr/bin/env bash

bin="../src/docbox"
[[ ! -f "$bin" ]] && bin="docbox"

bg_dir="$(dirname "$(readlink -f "$bin")")/lib/bg"

for n in default art picture delta delta2; do
	"$bin" -P "preset=$n" -S "toc-title=目  录" md2pdf sample.md && mv sample.md.pdf "sample.md.$n.pdf"
done

ls "$bg_dir" | while read -r n; do
	"$bin" -P "preset=picture;bg-image=$bg_dir/$n" -S "toc-title=目  录" md2pdf sample.md \
		&& mv sample.md.pdf "sample.picture.$n.pdf"
done

for n in 01 02 03 04 05 06 07 11 21 31; do
	"$bin" -p "$n" md2pdf sample.md && mv sample.md.pdf "sample.p${n}.pdf"
done

"$bin" md2pdf sample.md
