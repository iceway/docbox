#!/bin/bash

curdir="$(dirname "$(readlink -f "$0")")"

docbox_version="$("$curdir"/src/docbox 2>&1 | awk '/^Version:/{print $2}')"

install_docbox() {
	sudo "$curdir"/src/install.sh
}

uninstall_docbox() {
	sudo rm -rf /opt/docbox/
	sudo rm -f /usr/local/bin/docbox

	# remove wkhtmltopdf and pandoc ???
	# sudo dpkg -r wkhtmltox pandoc pandoc-plantuml-filter

}

pack_release_tarball() {
	cp -rf "$curdir/src" "$curdir/docbox" &&
		tar -czf "$curdir/docbox-$docbox_version.tar.gz" -C "$curdir" docbox &&
		rm -rf "$curdir/docbox"
}

usage() {
	cat <<EOF
Usage:  setup.sh --build|--install

  --install	install tools (pandoc/wkhtmltopdf), fonts (noto-cjk/sarasa-mono-sc)
        	and docbox (script/web_resource) to local system.
  --uninstall	uninstall previouse installed files.
  --release	pack a tarball to release, just need unpack it to /opt and run
		/opt/docbox/install.sh
EOF
}

main() {
	case "$1" in
	--install)
		install_docbox
		;;
	--uninstall)
		uninstall_docbox
		;;
	--release)
		pack_release_tarball
		;;
	*)
		usage
		;;
	esac
}

main "$@"
